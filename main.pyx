from cpython cimport array

cdef binary_search_float(array.array data, int low , int high ,float item):
    cdef int mid 
    if low <= high :
        mid = (low+high)//2
        if data[mid]==item:
            return mid
        elif item< data[mid]:
            return binary_search_float(data,low,mid-1,item)
        else:
            return binary_search_float(data,mid+1,high,item)
    else:
        return False 

cdef binary_search_int(array.array data, int low , int high ,int item):
    cdef int mid 
    if low <= high :
        mid = (low+high)//2
        if data[mid]==item:
            return mid
        elif item< data[mid]:
            return binary_search_float(data,low,mid-1,item)
        else:
            return binary_search_float(data,mid+1,high,item)
    else:
        return False 


cdef binary_search_string(list data, int low , int high ,str item):
    cdef int mid 
    if low <= high :
        mid = (low+high)//2
        if data[mid]==item:
            return mid
        elif item< data[mid]:
            return binary_search_string(data,low,mid-1,item)
        else:
            return binary_search_string(data,mid+1,high,item)
    else:
        return -1

cpdef return_fun_binary_search_float(array.array data, int low , int high ,float item):
    return  int(binary_search_float(data,low,high,item))

cpdef return_fun_binary_search_string(list data, int low , int high ,str item):
    return  (binary_search_string(data,low,high,item))

cpdef return_fun_binary_serch_int(array.array data,int low ,int high , int item):
  return binary_search_int(data , low , high , item )
