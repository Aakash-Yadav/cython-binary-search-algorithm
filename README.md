# Cython Recursive Binary Search Algorithm

_Using power of C Binary Search function in Python3 
to get best performance testing  and speed on Big data handling_


**Command To Run :**> python3 setup.py build_ext --inplace


#For More information check out: [Press](https://cython.org/)

### What is Recursive Binary:/?
Recursive binary search is an implementation of the binary search algorithm that uses recursive method calls (instead of iteratively searching for the item within a single method call).

### What benefit is there to using recursive binary search over iterative binary search ?
In a recent assignment for my programming 2 class we tested the efficiency of searches by populating a java ArrayList with 13,040 strings. The sequential search was obviously slower than the binary searches due to the complexity difference and the amount of times the code actually has to loop through the code. 
Iterative binary search and recursive binary search, however, had the same amount of comparisons. For example:

### _Example_

![alt text](https://xp.io/storage/1AEPWBIc.png)

### **Explanation**

- [ ] A function named 'binary_search' is defined.
- [ ] > It takes the list, the 'low' variable, 'high' variable and the element to be searched as parameter.
- [ ] > Then, the variable 'mid' is assigned the average of 'high' and 'low' variables.
- [ ] > If the element at 'mid' is same as the element that needs to be searched for, it is returned.
- [ ] > Else, if the element at 'mid' position is greater than the element to be searched, the function is called again by passing different set of parameters.
- [ ] > Else if the element at 'mid' position is less than the element to be searched, the function is called again by passing a different set of parameters.
- [ ] > Now the list is defined, and the function is called by passing this list as parameter.
- [ ] > This operation's data is stored in a variable.
- [ ] > This variable is the output that is displayed on the console.





